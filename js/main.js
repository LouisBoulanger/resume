var pagelist;
var navlinklist;
var skillsOpened;
var portfolioAttained = false;
var gifs = [
	"img/todo.gif",
	"img/todo3d.gif",
	"img/trinsitu.gif",
	"img/fnac.gif",
	"img/this.gif",
	"img/wikisearch.gif"
];
var currentGif = -1;

$(document).ready(function() {
	$("html", "body").scrollTop(0);
	pagelist = $(".page");
	navlinklist = $("#nav-top a");
	$("body").css("overflow", "hidden");
	$(window).on("scroll", fixDiv);
	setTimeout(begin,2300);

	$(".formation").hover(function() {
		if ($(this).children(".formation-details-container").length === 0)
		{
			var div = $("<div/>").addClass("formation-details-container").appendTo($(this));
			var h4 = $("<h4/>").addClass("details-title").html($(this).attr("formation-name")).appendTo(div);
			var dates = $("<p/>").addClass("details-dates").html($(this).attr("dates") + " @ " + $(this).attr("school")).appendTo(div);
			var p = $("<p/>").addClass("details-texte").html($(this).attr("details")).appendTo(div);
			div.animate({top: "120px", opacity: 1}, 400);
		}
	}, function() {
		if ($(this).children(".formation-details-container").length > 0)
		{
			if (!($(this).hasClass('selected')))
				fadeOutDetails($(this).children(".formation-details-container"));
		}
	});

	$(".formation").click(function() {
		ref = $(this);
		$(".formation").each(function() {
			if (ref[0] !== $(this)[0])
			{
				if ($(this).children(".formation-details-container").length > 0)
				{
					fadeOutDetails($(this).children(".formation-details-container"));
				}
			}
			$(this).removeClass("selected");
		});
		$(this).addClass('selected');
	});

	$(document).scroll(function() {
		if (!portfolioAttained)
		{
			if ($(window).scrollTop() >= $("#portfolio-page").position().top)
			{
				portfolioAttained = true;
				for (var i = 0; i < $(".project").length; i++)
				{
					var project = $(".project:nth-child(" + (i + 1) + ")");
					setTimeout(showProject.bind(null, project), 200 * i);
				}
			}
		}

		pagelist.each(function() {
			var ref = $(this);
			if ($(window).scrollTop() >= $(this).position().top)
			{
				navlinklist.each(function() {
					$(this).removeClass("selected");
				});

				var link;
				for(var l of navlinklist)
				{
					if ($(l).attr("href") === "#" + ref.attr("id"))
						link = l;
				}
				if ($(link).attr("href") !== "#main-page")
					$(link).addClass("selected");
			}
		});
	});

	$(".skill-section-title").click(function() {
		var ref = $(this);
		$(".skill-section-title").removeClass('selected');
		$(this).addClass('selected');
		if (!skillsOpened)
		{
			skillsOpened = true;
			$("#skills-container").animate({maxHeight: "60vh"}, 400, function() {
				$("#" + ref.attr("id") + "-section").removeClass('hidden')
				.animate({opacity: 1}, 400);
			});
		}
		else
		{
			var other = $("#skills-container").children(".hidden");
			var current = $("#skills-container").children(":not(.hidden)");
			if (current.attr("id") !== $(this).attr("id")  + "-section")
			{
				current.animate({opacity: 0}, 400, function() {
					$(this).addClass('hidden');
					other.removeClass('hidden').animate({opacity: 1}, 400);
				});
			}
		}
	});

	$(".skill").click(function() {
		if (!$(this).hasClass('skill-selected'))
		{
			$(".skill").removeClass('skill-selected');
			$(this).addClass('skill-selected');
			$(".skill").css("flex-basis", "0%").each(function() {
				$(this).children(".skill-content").css("opacity", "0");
				$(this).children("i").css("opacity", "1");
			});
			$(this).css("flex-basis", "35%");
			$(this).children("i").css("opacity", "0");
			var ref = $(this);
			setTimeout(function() {
				ref.children(".skill-content").css("opacity", "1");
			}, 400);
		}
	});

	$(".noted-skill-bar").hover(function() {
		var ref = $(this).children(".bar-filling");
		console.log(ref);
		if (ref.children("p").length === 0)
		{
			if (ref.hasClass('bar-filling-meh'))
			{
				$("<p/>").html("Meh...").addClass('noted-skill-note').appendTo(ref).animate({opacity: 1}, 100);
			}
			else if (ref.hasClass('bar-filling-ok'))
			{
				$("<p/>").html("Good").addClass('noted-skill-note').appendTo(ref).animate({opacity: 1}, 100);
			}
			else if (ref.hasClass('bar-filling-experienced'))
			{
				$("<p/>").html("Experienced").addClass('noted-skill-note').appendTo(ref).animate({opacity: 1}, 100);
			}
			else if (ref.hasClass('bar-filling-proficient'))
			{
				$("<p/>").html("Proficient").addClass('noted-skill-note').appendTo(ref).animate({opacity: 1}, 100);
			}
		}
	}, function() {
		var ref = $(this).children(".bar-filling");
		if (ref.children("p").length > 0)
		{
			setTimeout(function() {
				ref.children("p").css("opacity", "0");
				setTimeout(function() {
					ref.children("p").remove();
				}, 400);
			}, 2000);
		}
	});

	$(".project").click(function() {
		if (!$(this).hasClass('project-full'))
		{
			$(".project").css("opacity", "0");
			var ref = $(this);
			setTimeout(function() {
				$(".project").each(function() {
					if ($(this).attr("id") != ref.attr("id"))
					{
						$(this).css("display", "none");
					}
				});
				ref.addClass("project-full").css("opacity", "1");
				$(".return").css("opacity", "1");
			}, 400);
		}
	});

	$(".return").click(function() {
		$(".project").css("opacity", "0");
		$(this).css("opacity", "0");
		setTimeout(function() {
			$(".project").removeClass("project-full").css("display", "block").css("opacity", "1");
		}, 400);
	});

	$("#contact-email").click(function() {
		$("#contact-page .page-title, .contact-links, .contact-links *").animate({opacity: 0}, 400, function() {
			$("#contact-page .page-title, .contact-links, .contact-links *").addClass('contact-hidden');
			$("#contact-form").removeClass('contact-form-hidden').css("display", "flex").css("flex-direction", "column").css("align-items", "center").animate({opacity: 1}, 400);
		});
	});

	$("#contact-form-return").click(function() {
		$("#contact-form").addClass('contact-form-hidden');
		setTimeout(function () {
			$("#contact-form").css("display", "none");
			$("#contact-page .page-title, .contact-links, .contact-links *").removeClass('contact-hidden').animate({opacity: 1}, 400);
		}, 400);
	});

	$("#contact-form input, textarea").click(function() {
		$(this).removeClass('invalid-form');
	});

	$("#sendmail").click(function() {
		var name = $("#mail-name").val();
		var mail = $("#mail-email").val();
		var msg  = $("#mail-message").val();

		if (name.length > 0 && mail.length > 0 && msg.length > 0)
		{
			sendMail(name, mail, msg);
		}

		else
		{
			if (name.length == 0)
				$("#mail-name").addClass('invalid-form');
			if (mail.length == 0)
				$("#mail-email").addClass('invalid-form');
			if (msg.length == 0)
				$("#mail-message").addClass('invalid-form');
		}
	});
});

function showProject(project)
{
	project.css("opacity", "1");
}

function begin()
{
	$("#media-cover").animate({marginBottom: 0}, 1000, function() {
		$("#round-photo").animate({opacity: 1}, 500);
		$("body").css("overflow-y", "scroll");
		changeWallpaper();
		setInterval(changeWallpaper, 4000);
	});
}

function changeWallpaper()
{
	if (++currentGif > gifs.length - 1)
		currentGif = 0;
	$("#media-cover").css("background-image", "url(" + gifs[currentGif] + ")");
}

function fixDiv() {
    var $div = $("#nav-top");
    if ($("#nav-placeholder").length > 0)
    {
	    if ($(window).scrollTop() <= $("#nav-placeholder").position().top)
	    {
	    	$("#nav-placeholder").stop().remove();
	    	$div.css({'position': 'static', 'top': 'auto', 'width': '100%'});
	        $div.children("#nav-top-left").stop().animate({paddingRight: "12vw"}, 400);
	    }
    }
    
    // Vers le bas
    if ($("#nav-placeholder").length > 0)
    {
    	if ($("#nav-placeholder").position().top > $div.position().top)
    	{
    		$div.css({'position': 'fixed', 'top': '0', 'width': '100%'});
	        if ($("#nav-placeholder").length === 0)
	        {
	        	$("<div/>").attr("id", "nav-placeholder").prependTo($("#about"));
	        }
	        $div.children("#nav-top-left").stop().animate({paddingRight: "0"}, 400);
    	}
    }
	else
	{
	    if ($(window).scrollTop() > $div.position().top) { 
	        $div.css({'position': 'fixed', 'top': '0', 'width': '100%'});
	        if ($("#nav-placeholder").length === 0)
	        {
	        	$("<div/>").attr("id", "nav-placeholder").prependTo($("#about"));
	        }
	        $div.children("#nav-top-left").stop().animate({paddingRight: "0"}, 400);
	        
	    }
	    else {
	        $div.css({'position': 'static', 'top': 'auto', 'width': '100%'});
	        $div.children("#nav-top-left").stop().animate({paddingRight: "12vw"}, 400);
	    }
	}
    
    
    // Vers le haut
    
}

function fadeOutDetails(div)
{
	div.animate({opacity: 0}, 400, function() {
		div.remove();
	});
}

function sendMail(name, mail, msg)
{
	var email = { "name": name, "mail": mail, "msg": msg };
	$.post(
		"mail.php",
		email,
		function(data) {
			
		},
		"post"
	);
}